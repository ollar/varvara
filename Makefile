

prod:
	gunicorn -D varvara.wsgi --bind 0.0.0.0:8090 --access-logfile app_access.log --error-logfile app_error.log


.PHONY: prod