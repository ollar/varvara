from varvara.base import views
from django.urls import include, path
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView
from .views import TokenObtainPairViewWithUserData


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('', include(router.urls), name='users'),
    path(r'register', views.create_user, name='register'),
    path('api/token/', TokenObtainPairViewWithUserData.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
]