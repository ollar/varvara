from django.contrib.auth.models import User
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from varvara.base.serializers import UserSerializer
from rest_framework.response import Response
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from rest_framework import permissions
from rest_framework_simplejwt.views import TokenObtainPairView
from .serializers import TokenObtainPairSerializerWithUserData

class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Api endpoint that allow users to be viewed or edited
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAdminUser]


@api_view(['POST'])
def create_user(request):
    serialized = UserSerializer(data=request.data)

    if serialized.is_valid():
        try:
            validate_password(request.data['password'])
        except ValidationError as e:
            return Response({
                'password': e.messages
            }, status=status.HTTP_400_BAD_REQUEST)

        password = make_password(request.data['password'])



        serialized.save(password=password)
        return Response(serialized.data, status=status.HTTP_201_CREATED)
    return Response(serialized.errors, status=status.HTTP_400_BAD_REQUEST)

"""
{
"username": "test",
"email": "olll@llla.co",
"password": "123456"
}
"""

class TokenObtainPairViewWithUserData(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializerWithUserData